I was one of those kids that enjoyed creating "hacks" for multiplayer and "trolling" people with them. Dark times...

This is used to spam the chat in multiplayer.

How to use:
Use the `setspammessage [put message here]` command in the console to set the message to spam.
Use the `spam` command in the console to toggle spamming.
Or press Right-CTRL + S to toggle spamming.

How to compile:
I lost the project files after a hard drive failure, so you'll need to:

* create a new C# library/DLL project in Visual Studio
* replace the generated cs file with the cs file in this archive
* download .NET ScriptHook and add a reference to it
* click compile
* change the extension of the generated .dll file to .net.dll
