﻿using GTA;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace ChatSpammer
{
    public class ChatSpammer_Script : Script
    {
        GTA.Timer timer1;
        bool spamMessage = false;
        string message = "This is a spam message, so, umm, yeah....";

        public ChatSpammer_Script()
        {
            BindConsoleCommand("setspammessage", setSpamMessage, "- [(string) spam message] - Sets the spam message");
            BindConsoleCommand("spam", toggleSpam, "- Toggles spamming");

            KeyDown += ChatSpammer_Script_KeyDown;
            timer1 = new GTA.Timer(500, false);
            timer1.Tick += timer1_Tick;
            timer1.Start();
            PerFrameDrawing += ChatSpammer_Script_PerFrameDrawing;
        }

        private void ChatSpammer_Script_PerFrameDrawing(object sender, GraphicsEventArgs e)
        {
            if (Game.MultiplayerGameMode != GameMode.None && spamMessage && !string.IsNullOrWhiteSpace(message)) e.Graphics.DrawText("Spamming '" + message + "'. You will not see the messages", 10, 10, Color.Red);
        }

        private void ChatSpammer_Script_KeyDown(object sender, GTA.KeyEventArgs e)
        {
            if (isKeyPressed(Keys.RControlKey) && e.Key == Keys.S && Game.MultiplayerGameMode != GameMode.None)
            {
                ToggleSpam();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (spamMessage && Game.MultiplayerGameMode != GameMode.None)
            {
                if (!string.IsNullOrWhiteSpace(message))
                {
                    NETWORK_SEND_TEXT_CHAT(Game.LocalPlayer, message);
                }
                else
                {
                    spamMessage = false;
                    Subtitle("Your spam message is empty. Please set another");
                }
            }
        }

        private void toggleSpam(ParameterCollection Parameter)
        {
            ToggleSpam();
        }

        private void setSpamMessage(ParameterCollection Parameter)
        {
            if (Game.isMultiplayer && Parameter.Count > 0)
            {
                string endString = "";
                for (int i = 0; i < Parameter.Count; i++)
                {
                    endString += Convert.ToString(Parameter[i]) + " ";
                }
                message = endString;
                Game.Console.Print("**Spam message = " + message);
            }
        }

        private void ToggleSpam()
        {
            spamMessage = !spamMessage;
            Subtitle("Spamming " + (spamMessage ? "started >:)" : "stopped"));
        }

        private void NETWORK_SEND_TEXT_CHAT(Player player, string message)
        {
            if (Game.isMultiplayer) GTA.Native.Function.Call("NETWORK_SEND_TEXT_CHAT", player, message);
        }

        private void Subtitle(string message, int time = 1500)
        {
            GTA.Native.Function.Call("PRINT_STRING_WITH_LITERAL_STRING_NOW", "STRING", message, time, 1);
        }
    }
}
